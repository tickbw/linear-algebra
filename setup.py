# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='linear',
    version='0.1.0',
    description='Sample package for Python-Guide.org',
    long_description=readme,
    author='Ben Wann',
    author_email='ben.wann@gmail.com',
    url='https://bitbucket.com:tickbw/linear-algebra.git',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

