# -*- coding: utf-8 -*-

from .context import linear

import unittest


class LinearAlgebraTestSuite(unittest.TestCase):
    """Advanced test cases."""

    def test_plus_simple(self):
        v = linear.Vector([8.218,-9.341])
        w = linear.Vector([-1.129,2,111])
        result = v.plus(w)
        print result
        self.assertEqual(linear.Vector([7.089, -7.341]), result.to_test_precision())

    def test_minus_simple(self):
        v = linear.Vector([3,2,1])
        w = linear.Vector([1,1,1])
        result = v.minus(w)
        self.assertEqual(linear.Vector([2,1,0]), result.to_test_precision())

if __name__ == '__main__':
    unittest.main()
